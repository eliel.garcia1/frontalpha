import React, { useState } from 'react';
import { Alert, Button } from "reactstrap";
import { getVersion } from '../services/proxy';
import Highlight from '../components/Highlight';
import { useAuth0, withAuthenticationRequired } from "@auth0/auth0-react";
import Loading from "../components/Loading";

export const CallApi = () => {
  let responseApi = "Test";
  const [state, setState] = useState({
    showResult: false,
    apiMessage: "",
    code: 0,
    error: null,
  });

  const {
    getAccessTokenSilently,
    loginWithPopup,
    getAccessTokenWithPopup,
  } = useAuth0();

  const handleConsent = async () => {
    try {
      await getAccessTokenWithPopup();
      setState({
        ...state,
        error: null,
      });
    } catch (error) {
      setState({
        ...state,
        error: error.error,
      });
    }

    await callApi();
  };

  const handleLoginAgain = async () => {
    try {
      await loginWithPopup();
      setState({
        ...state,
        error: null,
      });
    } catch (error) {
      setState({
        ...state,
        error: error.error,
      });
    }

    await callApi();
  };

  const handle = (e:any, fn:any) => {
    e.preventDefault();
    fn();
  };

  const callApi = async () => {
    try {
      //Obtiene el token generado por Auth
      const token = await getAccessTokenSilently();
      console.log(token);
      //Consume el backend
      return getVersion("2.1.7")
        .then((result) => {
          if (result.codigoRespuesta) {
            responseApi = result.mensaje;
            console.log("responseApi = ", responseApi);
            setState({
              ...state,
              showResult: true,
              code: result.codigoRespuesta,
              apiMessage: responseApi,
            });
            console.log(state.showResult);
          }
        });
    } catch (error) {
      setState({
        ...state,
        error: error.error,
      });
    }
  };


  return (
    <div>
      <div className="mb-5">
        {state.error === "consent_required" && (
          <Alert color="warning">
            You need to{" "}
            <a
              href="#/"
              className="alert-link"
              onClick={(e) => handle(e, handleConsent)}
            >
              consent to get access to users api
            </a>
          </Alert>
        )}

        {state.error === "login_required" && (
          <Alert color="warning">
            You need to{" "}
            <a
              href="#/"
              className="alert-link"
              onClick={(e) => handle(e, handleLoginAgain)}
            >
              log in again
            </a>
          </Alert>
        )}
      </div>
      <h1>External API</h1>
      <p className="lead">
        Ping an external API by clicking the button below.
        </p>

      <p>
        This will call an external API, developed in python, which will serve as a base example to integrate all the services that your project requires.
        </p>
      <div className="container d-flex justify-content-start">
        <div className="row">
          <div className="col">
            <Button
              color="primary"
              onClick={callApi}
            >Call API</Button>
          </div>
          <div className="col" data-testid="api-result">

            {state.showResult &&
              (
                <Highlight>
                  <code>
                    {JSON.stringify(state.apiMessage, null, 2)}
                  </code>
                </Highlight>
              )
            }
          </div>
          <div className="col">
            {
              (state.showResult && state.code === 200) ?
                (
                  <Alert color="success">
                    <code>Http Code : {state.code}</code>
                  </Alert>
                ) :
                state.code > 0 && (
                  <Alert color="warning">
                    <code>HttpCode {state.code}</code>
                  </Alert>
                )
            }
          </div>
        </div>
      </div>
    </div>

  )

}

export default withAuthenticationRequired(CallApi, {
  onRedirecting: () => <Loading />,
});
