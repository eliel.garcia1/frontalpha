import { ENV } from "./env";

export const getEndpointBase = () => {
    switch(ENV){
        case 'DEV':
            return 'https://servicesdev.hefesto-alphacredit.co'
        case 'QA':
            return 'endpoint QA'
        case 'PRE':
            return 'endpoint Pre-Production'
        case 'PRO':
            return 'endpoint Production'
    }
  };
  