import { getEndpointBase } from "../config/config";

export type Response = {
  codigoRespuesta: number;
  mensaje: string;
  codigoTransaccion: number;
  data: Array<any>;
  token: string;
}
export const getVersion = (version: string) => new Promise<Response>(async (resolve, reject) => {
  return fetch(`${getEndpointBase()}/security/api/VersionControl/ValidateVersion/${version}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  }).then(res => res.json())
    .catch(error => reject(error))
    .then(response => resolve(response));
})

export const getAdoResult = (id: string) => new Promise<Response>(async (resolve, reject) => {
  const data = {
    "idConsultaAdo": id,
    "tipo": 2,
    "idTipoDispositivo": 2
  }
  return fetch(`${getEndpointBase()}/security/api/CognitiveServices/FindByNumberId`, {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    }
  }).then(res => res.json())
    .catch(error => reject(error))
    .then(response => resolve(response));
})